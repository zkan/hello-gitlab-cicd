import unittest

from my_module import sum


class TestMyModule(unittest.TestCase):
    def test_sum(self):
        result = sum(1, 2)
        self.assertEqual(result, 3)


unittest.main()
